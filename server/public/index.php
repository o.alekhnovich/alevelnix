<?php
/**
 * @param  array $arr
 * @return boolean
 */
function dd(array $arr): bool
{
    // phpcs:disable
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
    // phpcs:enable
    return true;
}
